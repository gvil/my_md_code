#include "hello.h"
#include <Eigen/Dense>
#include <iostream>
#include "xyz.h"
#include "lj_direction_summation.h"
#include "verlet.h"
#include "neighbors.h"
#include "gupta.h"
#include "logger.h"
#include "stress.h"
#include "thermostat.h"

#define USE_MPI
#ifdef USE_MPI
#include <mpi.h>
#include "domain.h"
#endif


int main(int argc, char *argv[]) {
    int rank = 0, size = 1;

    // Below is some MPI code, try compiling with `cmake -DUSE_MPI=ON ..`
#ifdef USE_MPI
    MPI_Init(&argc, &argv);

    // Retrieve process infos
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    Domain domain(MPI_COMM_WORLD, {50, 50, 144.24978336}, {1, 1, 16}, {0, 0, 1});
#endif

    std::cout << "Hello I am rank " << rank << " of " << size << "\n";

    auto [names, positions, velocities]{read_xyz_with_velocities("data/whisker_small.xyz")};
    Atoms my_atoms = Atoms(positions);


    std::ofstream traj("data/traj.xyz");
    std::ofstream log("data/log.csv");

    double timestep = 1;
    double cutoff = 5.0;

    NeighborList my_neighbors(cutoff);
    my_neighbors.update(my_atoms);

    domain.enable(my_atoms);

    domain.exchange_atoms(my_atoms);

    domain.update_ghosts(my_atoms, 2 * cutoff);
    for (int j = 0; j < 2000; j++) {
        my_neighbors.update(my_atoms);
        domain.exchange_atoms(my_atoms);
        domain.update_ghosts(my_atoms, 2 * cutoff);
        verlet1(my_atoms, timestep);
        gupta(my_atoms, my_neighbors, cutoff);
        verlet2(my_atoms, timestep);

        double temp = berendsen_thermostat(my_atoms, 100, timestep, 100);
        std::cout << temp << std::endl;
    }

    for (int i = 0; i < 1000; i++) {

        domain.scale(my_atoms, Eigen::Array3d(90.0, 90.0, domain.domain_length()[2] * 1.001));

        for (int j = 0; j < 2000; j++) {
            my_neighbors.update(my_atoms);
            domain.exchange_atoms(my_atoms);
            domain.update_ghosts(my_atoms, 2 * cutoff);
            verlet1(my_atoms, timestep);
            gupta(my_atoms, my_neighbors, cutoff);
            verlet2(my_atoms, timestep);

            berendsen_thermostat(my_atoms, 100, timestep, 100);
        }


        my_neighbors.update(my_atoms);
        verlet1(my_atoms, timestep);
        domain.exchange_atoms(my_atoms);
        domain.update_ghosts(my_atoms, 2 * cutoff);

        double pot_energy = 0;
        double kin_energy = 0;
        double stress = 0;

        my_neighbors.update(my_atoms);
        pot_energy = gupta(my_atoms, my_neighbors, cutoff, domain.nb_local());
        stress = stress_z(my_atoms, domain);
        verlet2(my_atoms, timestep);

        kin_energy = kinetic_energy(my_atoms);




        if (rank == 0) {
            std::cout << i * timestep << std::endl;
            write_xyz(traj, my_atoms);
            save(log, i , 0, 0, stress);
            std::cout << "pot: " << pot_energy << " kin: " << kin_energy
                      << " tot: " << pot_energy + kin_energy << std::endl;
        }
    }
    //domain.disable(my_atoms);
    traj.close();
    log.close();

#ifdef USE_MPI
    MPI_Finalize();
#endif

    return 0;
}
