#include "hello.h"
#include <Eigen/Dense>
#include <iostream>
#include "xyz.h"
#include "gupta.h"
#include "verlet.h"
#include "thermostat.h"
#include "neighbors.h"
#include "logger.h"


#ifdef USE_MPI
#include <mpi.h>
#endif


int main(int argc, char *argv[]) {
    int rank = 0, size = 1;

    // Below is some MPI code, try compiling with `cmake -DUSE_MPI=ON ..`
#ifdef USE_MPI
    MPI_Init(&argc, &argv);

    // Retrieve process infos
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
#endif

    std::cout << "Hello I am rank " << rank << " of " << size << "\n";

    auto [names, positions, velocities]{read_xyz_with_velocities("data/cluster_T400_1.xyz")};
    Atoms my_atoms = Atoms(names, positions, velocities);

    double cutoff = 5;

    NeighborList my_neighbors(cutoff);

    std::ofstream traj("data/traj.xyz");
    std::ofstream log("data/log.csv");

    double timestep = 1;


    for (int i = 0; i < 100; i++) {

        my_atoms.velocities *= sqrt((10 / kinetic_energy(my_atoms)) + 1);
        double  kin_tot = 0;
        double pot_energy = 0;

        for (int j = 0; j < 2000; j++) {
            my_neighbors.update(my_atoms);
            verlet1(my_atoms, timestep);
            pot_energy = gupta(my_atoms, my_neighbors, cutoff);
            verlet2(my_atoms, timestep);

            // double temp = berendsen_thermostat(my_atoms, 400, timestep, 100);

            if (j >= 1000) {
                kin_tot += kinetic_energy(my_atoms);
            }
        }
        double kin_energy = kinetic_energy(my_atoms);
        double avg_temp = (kin_tot/1000) / ((3.0 / 2.0) * 8.617333262e-5 * my_atoms.nb_atoms());
        std::cout << i * timestep << std::endl;
        write_xyz(traj, my_atoms);
        save(log, i * timestep*3000, pot_energy, kin_energy, avg_temp);

        std::cout << "pot: " << pot_energy << " kin: " << kin_energy
                  << " tot: " << pot_energy + kin_energy
                  << " temp: " << avg_temp << std::endl;

    }
    traj.close();
    log.close();

#ifdef USE_MPI
    MPI_Finalize();
#endif

    return 0;
}
