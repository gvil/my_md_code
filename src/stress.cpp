//
// Created by Georg on 25.03.2023.
//

#include "stress.h"

double stress_z(Atoms &atoms, Domain &domain)
{
    auto border = domain.domain_length()[2] / domain.size() * domain.coordinate()[2];

    double stress = 0;
    for (int i = domain.nb_local(); i < atoms.forces.cols(); i++) {
        if (atoms.positions(2, i) <= border) {
            stress += atoms.forces(2, i);
        }
    }

    return stress;
}