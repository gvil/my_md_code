//
// Created by Georg on 10.03.2023.
//

#ifndef MY_MD_CODE_TYPES_H
#define MY_MD_CODE_TYPES_H

#include <Eigen/Dense>

using Positions_t = Eigen::Array3Xd;
using Velocities_t = Eigen::Array3Xd;
using Forces_t = Eigen::Array3Xd;
using Masses_t = Eigen::ArrayXd;
using Names_t = Eigen::Vector<std::string, Eigen::Dynamic>;

#endif // MY_MD_CODE_TYPES_H
