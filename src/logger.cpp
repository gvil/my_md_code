//
// Created by Georg on 14.03.2023.
//
#include "logger.h"

void save(std::ofstream &file, double time, double pot, double kin, double temp)
{
    file << time << ";" << pot << ";" << kin << ";" << pot + kin << ";" << temp << ";" << std::endl;
}