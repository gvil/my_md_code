//
// Created by Georg on 10.03.2023.
//

#include "Atoms.h"
#include <iostream>

void Atoms::cubic_lattice(double grid)
{
    velocities.setZero();
    forces.setZero();
    int size = ceil(pow(positions.cols(), 1/3.0));
    int i = 0;
    for(int x = 0; x < size; x++){
        for(int y = 0; y < size; y++){
            for(int z = 0; z < size; z++){
                positions.col(i) = Eigen::Array3d(x*grid, y*grid, z*grid);
                i++;
                if(i >= positions.cols())
                {
                    return;
                }
            }
        }
    }
}


void Atoms::resize(int size)
{
    positions.conservativeResize(Eigen::NoChange, size);
    velocities.conservativeResize(Eigen::NoChange, size);
    forces.conservativeResize(Eigen::NoChange, size);
    masses.conservativeResize(size);
}
