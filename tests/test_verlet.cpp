//
// Created by Georg on 10.03.2023.
//

#include <gtest/gtest.h>
#include "verlet.h"
#include "Atoms.h"

TEST(verletTest, zeroSteps)
{
    //add the atom
    Atoms my_test_atom(1);
    my_test_atom.masses << 1.008;
    my_test_atom.positions << -0.5, 0, 1;
    my_test_atom.velocities<< 0.1, 1, -0.3;

    //set const forces
    Eigen::Array<double, 3, Eigen::Dynamic> const_force(3, 1);
    const_force << -1, 0.5, 0;


    //set timestep
    double timestep = 0.01;

    //test after 0 steps
    EXPECT_FLOAT_EQ(my_test_atom.masses(0), 1.008);
    EXPECT_FLOAT_EQ(my_test_atom.positions(0,0), -0.5);
    EXPECT_FLOAT_EQ(my_test_atom.positions(1,0), 0);
    EXPECT_FLOAT_EQ(my_test_atom.positions(2,0), 1);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(0,0), 0.1);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(1,0), 1);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(2,0), -0.3);


}

TEST(verletTest, oneStepHydrogen)
{
    //add the atom
    Atoms my_test_atom(1);
    my_test_atom.masses << 1.008;
    my_test_atom.positions << -0.5, 0, 1;
    my_test_atom.velocities<< 0.1, 1, -0.3;

    //set const forces
    Eigen::Array<double, 3, Eigen::Dynamic> const_force(3, 1);
    const_force << -1, 0.5, 0;

    //set timestep
    double timestep = 0.01;


    //step
    verlet1(my_test_atom, timestep);
    verlet2(my_test_atom, timestep);

    //test after 0 steps
    EXPECT_FLOAT_EQ(my_test_atom.masses(0), 1.008);
    EXPECT_FLOAT_EQ(my_test_atom.positions(0,0), -0.49900001);
    EXPECT_FLOAT_EQ(my_test_atom.positions(1,0), 0.0099999998);
    EXPECT_FLOAT_EQ(my_test_atom.positions(2,0), 0.997);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(0,0), 0.1);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(1,0), 1);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(2,0), -0.3);

}

TEST(verletTest, oneStepHGold)
{
    //add the atom
    Atoms my_test_atom(1);
    my_test_atom.masses << 196.97;
    my_test_atom.positions << -1, -2, 0;
    my_test_atom.velocities<< -0.5, 0, 1;

    //set const forces
    Eigen::Array<double, 3, Eigen::Dynamic> const_force(3, 1);
    const_force << -1, 0.5, 0;


    //set timestep
    double timestep = 0.1;


    //step
    verlet1(my_test_atom, timestep);
    verlet2(my_test_atom, timestep);

    EXPECT_FLOAT_EQ(my_test_atom.masses(0), 196.97);
    EXPECT_FLOAT_EQ(my_test_atom.positions(0,0), -1.05);
    EXPECT_FLOAT_EQ(my_test_atom.positions(1,0), -2);
    EXPECT_FLOAT_EQ(my_test_atom.positions(2,0), 0.1);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(0,0), -0.5);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(1,0), 0);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(2,0), 1);

}

TEST(verletTest, tenSteps)
{
    //add the atom
    Atoms my_test_atom(1);
    my_test_atom.masses << 1.008;
    my_test_atom.positions << -0.5, 0, 1;
    my_test_atom.velocities<< 0.1, 1, -0.3;

    //set const forces
    Eigen::Array<double, 3, Eigen::Dynamic> const_force(3, 1);
    const_force << -1, 0.5, 0;

    //set timestep
    double timestep = 0.01;

    for(unsigned int i = 0; i < 10; i++)
    {
        //step
        verlet1(my_test_atom, timestep);
        verlet2(my_test_atom, timestep);
    }


    //test after 0 steps
    EXPECT_FLOAT_EQ(my_test_atom.masses(0), 1.008);
    EXPECT_FLOAT_EQ(my_test_atom.positions(0,0), -0.49000001);
    EXPECT_FLOAT_EQ(my_test_atom.positions(1,0), 0.1);
    EXPECT_FLOAT_EQ(my_test_atom.positions(2,0), 0.97);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(0,0), 0.1);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(1,0), 1);
    EXPECT_FLOAT_EQ(my_test_atom.velocities(2,0), -0.3);


}