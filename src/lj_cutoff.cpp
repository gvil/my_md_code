//
// Created by Georg on 13.03.2023.
//

#include "lj_cutoff.h"

double lj_direct_summation_cutoff(Atoms &atoms, double cutoff, double epsilon, double sigma)
{
    NeighborList neighbor_list(cutoff);
    neighbor_list.update(atoms);

    double pot_energy = 0.0;

    for (auto[i, j]: neighbor_list) {
        if (i < j) {

            double distance =
                (atoms.positions.col(i) - atoms.positions.col(j))
                    .matrix()
                    .norm();

            // Compute the new energy
            double pauli_energy = pow(sigma / distance, 12.0);
            double london_energy = pow(sigma / distance, 6.0);
            pot_energy += 4.0 * epsilon * (pauli_energy - london_energy);


            Eigen::Vector3d single_force =
                24 * (epsilon/distance) * (2* pauli_energy - london_energy) *
                (atoms.positions.col(i) - atoms.positions.col(j))
                    .matrix()
                    .normalized();

            atoms.forces.col(i) += single_force.array();

        }

    }
    return 0.5 * pot_energy;
}