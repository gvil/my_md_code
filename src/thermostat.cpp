//
// Created by Georg on 13.03.2023.
//

#include "thermostat.h"
#include <iostream>

double berendsen_thermostat(Atoms &atoms, double temperature, double timestep, double relaxation_time)
{
    double is_temp = kinetic_energy(atoms) / ((3.0 / 2.0) * 8.617333262e-5 * atoms.nb_atoms());

    double lambda = std::sqrt(1.0 + (temperature / is_temp - 1.0) * timestep / relaxation_time);

    atoms.velocities *= lambda;
    return is_temp;
}
