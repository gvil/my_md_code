#include "hello.h"
#include <Eigen/Dense>
#include <iostream>
#include "xyz.h"
#include "lj_direction_summation.h"
#include "verlet.h"
#include "logger.h"


#ifdef USE_MPI
#include <mpi.h>
#endif


int main(int argc, char *argv[]) {
    int rank = 0, size = 1;

    // Below is some MPI code, try compiling with `cmake -DUSE_MPI=ON ..`
#ifdef USE_MPI
    MPI_Init(&argc, &argv);

    // Retrieve process infos
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
#endif

    std::cout << "Hello I am rank " << rank << " of " << size << "\n";

    auto [names, positions, velocities]{read_xyz_with_velocities("data/lj54.xyz")};
    Atoms my_atoms = Atoms(positions);

    std::ofstream traj("data/traj.xyz");
    std::ofstream log("data/loglj04.csv");

    double epsilon = 1.0;
    double sigma = 1.0;
    double timestep = 0.4;



    for (int i = 0; i < 10000; i++) {
        verlet1(my_atoms, timestep);
        double pot_energy = lj_direct_summation(my_atoms, epsilon, sigma);
        verlet2(my_atoms, timestep);


        std::cout << i * timestep << std::endl;
        write_xyz(traj, my_atoms);
        double kin_energy = kinetic_energy(my_atoms);
        std::cout << "pot: " << pot_energy  << " kin: " << kin_energy << " tot: " << pot_energy + kin_energy << std::endl;
        save(log, i, pot_energy, kin_energy, 0);
    }

    traj.close();
    log.close();

#ifdef USE_MPI
    MPI_Finalize();
#endif

    return 0;
}
