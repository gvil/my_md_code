//
// Created by Georg on 10.03.2023.
//

#ifndef MY_MD_CODE_ATOMS_H
#define MY_MD_CODE_ATOMS_H

#include "types.h"
#include <iostream>

class Atoms {
  public:
    Positions_t positions;
    Velocities_t velocities;
    Forces_t forces;
    Masses_t masses;

    Atoms(const Positions_t &p) :
          positions{p}, velocities{3, p.cols()}, forces{3, p.cols()}, masses{p.cols()} {
        velocities.setZero();
        forces.setZero();
        masses.setOnes();
        masses = masses*103.6;
    }

    Atoms(const Positions_t &p, const Velocities_t &v) :
          positions{p}, velocities{v}, forces{3, p.cols()}, masses{p.cols()} {
        assert(p.cols() == v.cols());
        forces.setZero();
        masses.setOnes();
        masses = masses*103.6;
    }

    Atoms(int nb_atoms)
        : positions{3, nb_atoms},
          velocities{3, nb_atoms},
          forces{3, nb_atoms},
          masses{nb_atoms} {
        positions.setZero();
        velocities.setZero();
        forces.setZero();
        masses.setOnes();
        masses = masses*103.6;
    }

    Atoms(const Names_t &n, const Positions_t &p) :
          positions{p}, velocities{3, p.cols()}, forces{3, p.cols()}, masses{p.cols()} {
        velocities.setZero();
        forces.setZero();
        masses.setOnes();
        for(int i = 0; i < n.size(); i++)
        {
            if(n(i) == "H"){masses(i) = 1.008;}
            if(n(i) == "Au"){masses(i) = 196.97;}
        }
        masses = masses*103.6;
    }

    Atoms(const Names_t &n, const Positions_t &p, const Velocities_t &v) :
          positions{p}, velocities{v}, forces{3, p.cols()}, masses{p.cols()} {
        forces.setZero();
        masses.setOnes();
        for(int i = 0; i < n.size(); i++)
        {
            if(n(i) == "H"){masses(i) = 1.008;}
            if(n(i) == "Au"){masses(i) = 196.97;}
        }
        masses = masses*103.6;
    }

    size_t nb_atoms() const {
        return positions.cols();
    }

    void cubic_lattice(double grid);

    void resize(int size);
};

#endif // MY_MD_CODE_ATOMS_H
