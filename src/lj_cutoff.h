//
// Created by Georg on 13.03.2023.
//

#ifndef MY_MD_CODE_LJ_CUTOFF_H
#define MY_MD_CODE_LJ_CUTOFF_H

#include "Atoms.h"
#include "neighbors.h"

double lj_direct_summation_cutoff(Atoms &atoms, double cutoff, double epsilon = 1.0, double sigma = 1.0);

#endif // MY_MD_CODE_LJ_CUTOFF_H
