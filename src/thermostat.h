//
// Created by Georg on 13.03.2023.
//

#ifndef MY_MD_CODE_THERMOSTAT_H
#define MY_MD_CODE_THERMOSTAT_H

#include "Atoms.h"
#include "lj_direction_summation.h"

double berendsen_thermostat(Atoms &atoms, double temperature, double timestep, double relaxation_time);

#endif // MY_MD_CODE_THERMOSTAT_H
