//
// Created by Georg on 25.03.2023.
//

#ifndef MY_MD_CODE_STRESS_H
#define MY_MD_CODE_STRESS_H

#include "Atoms.h"
#include "domain.h"

double stress_z(Atoms &atoms, Domain &domain);


#endif // MY_MD_CODE_STRESS_H
