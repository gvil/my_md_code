//
// Created by Georg on 10.03.2023.
//

#include "verlet.h"
#include <iostream>

void verlet1(Atoms &atoms, double timestep)
{
    atoms.velocities += atoms.forces.rowwise() / atoms.masses.transpose() * 0.5 * timestep;
    atoms.positions += atoms.velocities * timestep;
}

void verlet2(Atoms &atoms, double timestep)
{
    atoms.velocities += atoms.forces.rowwise() / atoms.masses.transpose() * 0.5 * timestep;
}