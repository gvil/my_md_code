//
// Created by Georg on 10.03.2023.
//

#ifndef MY_MD_CODE_LJ_DIRECTION_SUMMATION_H
#define MY_MD_CODE_LJ_DIRECTION_SUMMATION_H

#include "Atoms.h"

double lj_direct_summation(Atoms &atoms, double epsilon = 1.0, double sigma = 1.0);

double kinetic_energy(Atoms &atoms, int nb_local = 0);

#endif // MY_MD_CODE_LJ_DIRECTION_SUMMATION_H
