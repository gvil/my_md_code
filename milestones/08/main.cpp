#include "hello.h"
#include <Eigen/Dense>
#include <iostream>
#include "xyz.h"
#include "lj_direction_summation.h"
#include "verlet.h"
#include "neighbors.h"
#include "gupta.h"
#include "logger.h"

#define USE_MPI
#ifdef USE_MPI
#include <mpi.h>
#include "domain.h"
#endif


int main(int argc, char *argv[]) {
    int rank = 0, size = 1;

    // Below is some MPI code, try compiling with `cmake -DUSE_MPI=ON ..`
#ifdef USE_MPI
    MPI_Init(&argc, &argv);

    // Retrieve process infos
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    Domain domain(MPI_COMM_WORLD, {50, 50, 50}, {1, 1, 8}, {0, 0, 1});
#endif

    std::cout << "Hello I am rank " << rank << " of " << size << "\n";

    auto [names, positions, velocities]{read_xyz_with_velocities("data/cluster_923.xyz")};
    Atoms my_atoms = Atoms(positions);

    std::ofstream traj("data/traj.xyz");
    std::ofstream log("data/log.csv");

    double timestep = 1;
    double cutoff = 5.0;

    NeighborList my_neighbors(cutoff);
    my_neighbors.update(my_atoms);

    domain.enable(my_atoms);

    domain.exchange_atoms(my_atoms);

    domain.update_ghosts(my_atoms, 2 * cutoff);

    for (int i = 0; i < 1000; i++) {
        //my_neighbors.update(my_atoms);
        verlet1(my_atoms, timestep);
        domain.exchange_atoms(my_atoms);
        domain.update_ghosts(my_atoms, 2 * cutoff);

        double pot_energy = 0;
        double kin_energy = 0;

        if(domain.nb_local() > 0) {
            my_neighbors.update(my_atoms);
            pot_energy = gupta(my_atoms, my_neighbors, cutoff, domain.nb_local());
            verlet2(my_atoms, timestep);

            kin_energy = kinetic_energy(my_atoms, domain.nb_local());
        }



        double pot_global;
        MPI_Reduce(&pot_energy, &pot_global, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

        double kin_global;
        MPI_Reduce(&kin_energy, &kin_global, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

        if (rank == 0) {
            std::cout << i * timestep << std::endl;
            write_xyz(traj, my_atoms);
            save(log, i * timestep, pot_global, kin_global, 0);
            std::cout << "pot: " << pot_global << " kin: " << kin_global
                      << " tot: " << pot_global + kin_global << std::endl;
        }
    }
    domain.disable(my_atoms);
    traj.close();
    log.close();

#ifdef USE_MPI
    MPI_Finalize();
#endif

    return 0;
}
