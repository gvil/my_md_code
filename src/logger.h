//
// Created by Georg on 14.03.2023.
//

#ifndef MY_MD_CODE_LOGGER_H
#define MY_MD_CODE_LOGGER_H
#include <fstream>

void save(std::ofstream &file, double time, double pot, double kin, double temp);

#endif // MY_MD_CODE_LOGGER_H
